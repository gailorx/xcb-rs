#![feature(globs)]

extern crate libc;
extern crate xlib;

pub use xcb::*;

mod xcb;
pub mod xlib_xcb;
